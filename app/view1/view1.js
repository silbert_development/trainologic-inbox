'use strict';

angular.module('myApp.view1', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view1', {
            templateUrl: 'view1/view1.html',
            controller: 'View1Ctrl'
        });
    }])

    .controller('View1Ctrl', ['$scope', function ($scope) {

        var contacts = [
            {
                id: 1,
                name: "Friends",
                type: "Group",
                contacts: [
                    {id: 2, name: "Udi", type: "Contact"},
                    {id: 3, name: "Tommy", type: "Contact"},
                    {
                        id: 6,
                        name: "Old Friends",
                        type: "Group",
                        contacts: [
                            {id: 7, name: "Itay", type: "Contact"},
                        ]
                    },
                ]
            },
            {
                id: 4,
                name: "Family",
                type: "Group",
                contacts: [
                    {id: 5, name: "Roni", type: "Contact"},
                ]
            },
            {id: 8, name: "Ori", type: "Contact"},
        ];

        $scope.elements = [];

        //Initialize the top of the tree
        $scope.init = function () {
            addElements(contacts, 0, 0, 0);
        };

        $scope.toggleElement = function(element, index)
        {
            //Toggle a group opening
            if (element.type == "Group") {
                element.open = !element.open;
                if (element.open) {
                    //Open the data and add
                    addElements(
                        getChildren(contacts, element.id),
                        ( index + 1 ),
                        element.id,
                        ( element.level + 1 )
                    );
                }
                else {
                    //Remove the data from DOM
                    removeElements(element.id);
                }
            }
        }

        function getChildren(tree, id) {
            for (var i = 0; i < tree.length; i++) {
                var current = tree[i];
                //Found te object wit te children
                if (current.id == id) {
                    return current.contacts;
                }
                if (current.type == "Group") {
                    //recursive look for children in current group
                    var children = getChildren(current.contacts, id);
                    if (children !== null) {
                        return children;
                    }
                }
            }
            return null;
        }

        function addElements(fromTree, startIndex, parentId, level) {
            for (var i = 0; i < fromTree.length; i++) {
                var data = fromTree[i];
                var element = {
                    id: data.id,
                    name: data.name,
                    type: data.type,
                    parentId: parentId,
                    level: level,
                    open: false,
                };
                $scope.elements.splice(startIndex + i, 0, element);
            }
        }

        function removeElements(parentId) {
            for (var i = 0; i < $scope.elements.length; i++) {
                if ($scope.elements[i].parentId == parentId) {
                    $scope.elements.splice(i, 1);
                    i--;//Go back one, becuase it is removed
                }
            }
        }


        $scope.init();

    }]);